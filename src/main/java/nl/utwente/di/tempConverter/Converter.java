package nl.utwente.di.tempConverter;

class Converter {
    public double getFTemp(double cTemp) {
        return 1.8 * cTemp + 32;
    }
}